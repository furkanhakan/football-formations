var players = {};
var addPlayerModal = document.getElementById('addPlayerModal');
var addPlayerModalClose = document.getElementById("addPlayerModalClose");
var playerDetailModalClose = document.getElementById("playerDetailModalClose");
var addPlayerButton = document.getElementById('addPlayerButton');
var addPlayerSelectItem = document.getElementById("addPlayerSelect");
var playerDetailModal = document.getElementById("playerDetailModal");
var position;


function AddPlayerModalShow(from) 
{
    position = from.value;
    addPlayerModal.querySelector("p").innerHTML = from.value + " Alanına eklemek istediğiniz oyuncuyu seçiniz.";
    addPlayerModal.style.display = "block";
}

function PlayerDetail(from) 
{
    var playerName = from.innerHTML;
    var playerId = from.id;
    playerDetailModal.querySelector("input").value = playerId;
    playerDetailModal.querySelector("#playerDetailId").innerHTML = "Oyuncu ID: " + playerId;
    playerDetailModal.querySelector("#playerDetailName").innerHTML = "Oyuncu Adı: " + playerName;
    playerDetailModal.style.display = "block";
}

addPlayerButton.onclick = function () 
{
    var playerName = addPlayerSelectItem.value;
    var playerId = addPlayerSelectItem.options[addPlayerSelectItem.selectedIndex].id;
    if (players[playerId]) {
        alert("Oyuncu Sahada Yerleştirilmiş");
    }
    else {
        players[playerId] = {
            "playerName": playerName,
            "playerPosition": position
        };
        var player = "<li id='" + playerId + "' class = 'player' onclick='PlayerDetail(this)'>" + playerName + "</li>";
        $("#" + position + " .players").prepend(player);
    }
    addPlayerModalClose.onclick();
}

// Player Delete
function PlayerDelete() 
{
    var deleteId = playerDetailModal.querySelector("input").value;
    $("#" + deleteId).remove();
    delete players[deleteId];
    playerDetailModal.style.display = "none";
}

// Modal Close
addPlayerModalClose.onclick = function () 
{
    addPlayerModal.style.display = "none";
}

playerDetailModalClose.onclick = function () 
{
    playerDetailModal.style.display = "none";
}

window.onclick = function (event) 
{
    if (event.target == addPlayerModal || event.target == playerDetailModal) {
        addPlayerModal.style.display = "none";
        playerDetailModal.style.display = "none";
    }
}